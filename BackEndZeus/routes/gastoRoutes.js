const router = require('express').Router()
const Gasto = require('../models/Gasto')

//criar

router.post('/', async (req,res)=>{

    const {valor, comida, vet, banho} = req.body
    const data = new Date().toLocaleDateString();

    if(!valor){
        res.status(422).json({error: 'Valor é obrigatório'})
        return
    }

    const gastos = {
        data,
        valor,
        comida,
        vet,
        banho,
    }

    try{

        await Gasto.create(gastos)

        res.status(201).json({msg: `Gasto inserido com sucesso`})

    }catch(error){
        res.status(500).json({error: error})
    }

})

//ler
router.get('/', async (req,res)=>{
    try{
        
        const gastos = await Gasto.find()

        res.status(200). json(gastos)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

/*router.get('/:id', async (req,res)=>{
    const id = req.params.id
    try{
        
        const people = await Person.findOne({ _id: id})

        res.status(200). json(people)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})*/

router.get('/:id', async (req,res)=>{
    const id = req.params.id

    try{
        
        const gastos = await Gasto.findOne({ _id: id})

        if(!gastos){
            res.status(422).json({error: 'Gasto não encontrado'})
            return
        }

        res.status(200).json(gastos)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

router.patch('/:id', async (req,res)=>{

    const id = req.params.id

    const {data, valor, comida, vet, banho} = req.body

    const gastos = {
        data,
        valor,
        comida,
        vet,
        banho,
    }
    
    try{

        const updatedGasto = await Gasto.updateOne({_id: id}, gastos)

        if(updatedGasto.matchedCount === 0){
            res.status(422).json({error: 'Gasto não encontrado'})
            return
        }

        res.status(200).json(gastos)

    }catch(error){
        res.status(500).json({error: error})
    }
})

router.delete('/:id', async (req,res)=>{
    
    const id = req.params.id

    const gastos = await Gasto.findOne({ _id: id})

    if(!gastos){
        res.status(422).json({error: 'Gasto não encontrado'})
        return
    }

    try{

        await Gasto.deleteOne({ _id: id})

        res.status(200).json({message: `Gasto foi apagado com sucesso`})

    }catch(error){
        res.status(500).json({error: error})
    }


})

module.exports = router;