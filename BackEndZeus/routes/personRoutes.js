const router = require('express').Router()
const Person = require('../models/Person')

//criar

router.post('/', async (req,res)=>{

    const {name, salary, approved} = req.body

    if(!name){
        res.status(422).json({error: 'Nome é obrigatório'})
        return
    }

    const person = {
        name,
        salary,
        approved
    }

    try{

        await Person.create(person)

        res.status(201).json({msg: `${person.name} inserido com sucesso`})

    }catch(error){
        res.status(500).json({error: error})
    }

})

//ler
router.get('/', async (req,res)=>{
    try{
        
        const people = await Person.find()

        res.status(200). json(people)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

/*router.get('/:id', async (req,res)=>{
    const id = req.params.id
    try{
        
        const people = await Person.findOne({ _id: id})

        res.status(200). json(people)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})*/

router.get('/:name', async (req,res)=>{
    const name = req.params.name

    try{
        
        const people = await Person.findOne({ name: name})

        if(!people){
            res.status(422).json({error: 'Usuário não encontrado'})
            return
        }

        res.status(200).json(people)
        
    }catch(error){
        res.status(500).json({error: error})
    }
})

router.patch('/:name', async (req,res)=>{

    const nameId = req.params.name

    const {name, salary, approved} = req.body

    const person = {
        name,
        salary,
        approved,
    }
    
    try{

        const updatedPerson = await Person.updateOne({name: nameId}, person)

        if(updatedPerson.matchedCount === 0){
            res.status(422).json({error: 'Usuário não encontrado'})
            return
        }

        res.status(200).json(person)

    }catch(error){
        res.status(500).json({error: error})
    }
})

router.delete('/:name', async (req,res)=>{
    
    const nameId = req.params.name

    const people = await Person.findOne({ name: nameId})

    if(!people){
        res.status(422).json({error: 'Usuário não encontrado'})
        return
    }

    try{

        await Person.deleteOne({name: nameId})

        res.status(200).json({message: `${nameId} foi apagado com sucesso`})

    }catch(error){
        res.status(500).json({error: error})
    }


})

module.exports = router;