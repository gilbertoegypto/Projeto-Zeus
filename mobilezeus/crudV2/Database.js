import AsyncStorage from '@react-native-async-storage/async-storage';

const API = 'http://10.50.185.56:5001/gastos'

import {Alert} from 'react-native'


async function saveItem(listItem) {
    //listItem._id = new Date().getTime();
    await fetch(API,{
        method: 'POST',
        body: JSON.stringify(listItem),
        headers:{
            "Content-Type": "application/json",
          },
    }).then((response) => response.json())
    .then((json) => console.log(json))
    .then(Alert.alert(`Item: ${listItem.title} foi salvo com sucesso! ${listItem.date}`));
}

async function editItem(listItem, _id) {
    await fetch(`${API}/${_id}`, {
        method: 'PATCH',
        body: JSON.stringify(listItem),
        headers: {
            "Content-Type": "application/json",
        },
      })
      .then((response) => response.json())
      .then((json) => console.log(json))
      .then(Alert.alert(`Item: ${listItem.title} foi alterado com sucesso!`));
}


async function getItems() {
    const res = await fetch(API).then((res)=>res.json()).catch((err)=> console.log(err))
    //console.log(`O objeto do DB é: ${Object.keys(res[0])}`)
    return Promise.resolve(res)
}

async function getItem(_id) {
    const savedItems = await getItems();
    return savedItems.find(item => item._id === _id);
}

async function deleteItem(_id) {
    await fetch(`${API}/${_id}`, {
        method: 'DELETE',
      }).then(Alert.alert(`Item excluído com sucesso!`));
      return Promise.resolve()
}

module.exports = {
    saveItem,
    editItem,
    getItems,
    getItem,
    deleteItem
}