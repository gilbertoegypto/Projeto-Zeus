import '../App.css';
import {useState, useEffect} from 'react'
import {BsTrash, BsBookmarkCheck, BsBookmarkCheckFill} from 'react-icons/bs'
import {FiEdit3} from 'react-icons/fi' 
import {MdAddCircleOutline} from 'react-icons/md'
import { PieChart } from 'react-minimal-pie-chart';
import {AiOutlineCloseCircle} from 'react-icons/ai'

//const API = "http://localhost:5000";
const API = "http://localhost:5001";
const DataBase = "/gastos";

export const data = [
  { title: 'Banho', value: 25, color: '#E38627' },
  { title: 'Veterinário', value: 25, color: '#C13C37' },
  { title: 'Ração', value: 25, color: '#6A2135' },
  { title: 'Outros', value: 25, color: '#6A5185' }
]

const TodoChart = (props) => {

    const [banho, setBanho] = useState(25);
    const [vet, setVet] = useState(25);
    const [comida, setComida] = useState(25);
    const [outro, setOutro] = useState(25);
    const [todosGlobal, setTodosGlobal] = useState([])
    const [todos, setTodos] = useState([]);
    const [loading, setLoaing] = useState(false);
    const [year, setYear] = useState(2022)
    const [month, setMonth] = useState("00")
    const [displayMonth, setDisplayMonth] = useState("00")
    const [displayYear, setDisplayYear] = useState(2022)

    //Carregar gastos no pageload
    useEffect(()=>{
      const loadData = async() =>{
        setLoaing(true);
        const res = await fetch(API + "/gastos")
        .then((res)=> res.json())
        .then((data)=>data)
        .catch((err)=>console.log(err))
        setLoaing(false);
        setTodosGlobal(res);
        setTodos(res.sort((a,b)=> new Date(b.date).getTime() - new Date(a.date).getTime()).filter(todo => todo.date.split('-')[0]===`${year}`))
        setYear(2022)
        setMonth("00")
        setBanho(25);
        setComida(25);
        setVet(25);
        setOutro(25);
        handleFilter(month, year)
      };
      loadData();
    },[])
    useEffect(()=>{
      getItems()
    },[props.refresh])

    const getItems = async() =>{
      const res = await fetch(API + "/gastos")
      .then((res)=> res.json())
      .then((data)=>data)
      .catch((err)=>console.log(err))
      setTodosGlobal(res);
      setTodos(res.sort((a,b)=> new Date(b.date).getTime() - new Date(a.date).getTime()).filter(todo => todo.date.split('-')[0]===`${year}`))

    }

    const handleMonth = () =>{
      if(month === "01"){
        setDisplayMonth('janeiro') 
      }else if(month === "02"){
        setDisplayMonth('fevereiro')
      }else if(month === "03"){
        setDisplayMonth('março') 
      }else if(month === "04"){
        setDisplayMonth('abril') 
      }else if(month === "05"){
        setDisplayMonth('maio') 
      }else if(month === "06"){
        setDisplayMonth('junho') 
      }else if(month === "07"){
        setDisplayMonth('julho') 
      }else if(month === "08"){
        setDisplayMonth('agosto') 
      }else if(month === "09"){
        setDisplayMonth('setembro') 
      }else if(month === "10"){
        setDisplayMonth('outubro') 
      }else if(month === "11"){
        setDisplayMonth('novembro') 
      }else if(month === "12"){
        setDisplayMonth('dezembro') 
      }else{
        setDisplayMonth("00")
      }
    }
    const handleFilter = (mes, ano)=>{
        if(month === "00"){
            setTodos(todosGlobal.filter(todo => todo.date.split('-')[0]===`${year}`))
        }else{
            setTodos(todosGlobal.filter(todo => todo.date.split('-')[1]===`${month}`).filter(todo => todo.date.split('-')[0]===`${year}`))
        }

        const banhos = todos.filter(todo => todo.banho===true).reduce((res,cur)=> res+cur.valorFormat,0).toFixed(2)
        const comidas = todos.filter(todo => todo.comida===true).reduce((res,cur)=> res+cur.valorFormat,0).toFixed(2)
        const vets = todos.filter(todo => todo.vet===true).reduce((res,cur)=> res+cur.valorFormat,0).toFixed(2)
        const outros = todos.filter(todo => todo.banho===false).filter(todo => todo.vet===false).filter(todo => todo.comida===false).reduce((res,cur)=> res+cur.valorFormat,0).toFixed(2)
        data[0].value = parseFloat(banhos)
        data[2].value = parseFloat(comidas)
        data[1].value = parseFloat(vets)
        data[3].value = parseFloat(outros)
        console.log(`${data[0].title} ${data[0].value}`)
        console.log(`${data[1].title} ${data[1].value}`)
        console.log(`${data[2].title} ${data[2].value}`)
        console.log(`${data[3].title} ${data[3].value}`)
    }
    const handlePercentual = (valor) =>{
      return ((valor/todos.reduce((res,cur)=> res+cur.valorFormat,0))*100)
    }

    const handleTotal =  ()=>{
      const total =  todos.reduce((res,cur)=> res+cur.valorFormat,0).toFixed(2)
      return total
    }

    const handleDate = (data) =>{
      const [ano,mes,dia] = data.split('-')
  
      return `${dia}/${mes}/${ano}`
    }
  
    const handleForm = () =>{
      props.setModalChart(false)
    }

    if(loading===true){
        return <div className="todo-chart"><p>Carregando...</p></div>
    }
    return(
        <div className="todo-chart">
            <div className="todo-chart-title">
            <AiOutlineCloseCircle onClick={handleForm}/>
            </div>
            <div className="todo-chart-one">
              {data[0].value===0&&data[1].value===0&&data[2].value===0&&data[3].value===0?
              <p>Não há dados!</p>
              :
              <PieChart className='pie-chart' data={data} animate={true} />
              }
            
            <div className='legenda'>
              <div className='vet'></div>Veterinário: {`R$:${data[1].value.toFixed(2)}`}
              <div className='banho'></div>Banho: {`R$:${data[0].value.toFixed(2)}`}
              <div className='comida'></div>Ração: {`R$:${data[2].value.toFixed(2)}`}
              <div className='outros'></div>Outros: {`R$:${data[3].value.toFixed(2)}`}
            </div>

            </div>
            <div className="todo-chart-filter">
                <h2>Filtro</h2>
                <select className="month-picking" onChange={(e)=>{setMonth(e.target.value)}}>
                <option value="00">Filtrar por mês</option>
                <option value="01">Janeiro</option>
                <option value="02">Fevereiro</option>
                <option value="03">Março</option>
                <option value="04">Abril</option>
                <option value="05">Maio</option>
                <option value="06">Junho</option>
                <option value="07">Julho</option>
                <option value="08">Agosto</option>
                <option value="09">Setembro</option>
                <option value="10">Outubro</option>
                <option value="11">Novembro</option>
                <option value="12">Dezembro</option>
                </select>
                <input className='year-picking' 
                type='number' min='1900' max= '2099' 
                step='1' defaultValue='2022'
                onChange={(e)=>{setYear(e.target.value)}}/>
                <button onClick={()=>handleFilter(month, year)} >Gerar</button>
            </div>

        </div>
        
    )

}

export default TodoChart